<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Psr\Log\LoggerInterface;
use App\Manager\FeedManager;
use App\Service\FeedReaderService;
use App\Entity\Feed;

class FeedController extends AbstractController
{
  private $logger;
  private $feedManager;
  private $feedReader;

  public function __construct(LoggerInterface $logger, FeedManager $feedManager, FeedReaderService $feedReader) {
    $this->logger = $logger;
    $this->feedManager = $feedManager;
    $this->feedReader = $feedReader;
  }

  /**
   * Controlador de la vista de lista.
   * @param Request $request
   * 
   * @Route("/", methods={"GET"}, name="list-feed")
   */
  public function listAction(Request $request)
  {
    $this->logger->info('Fired '.__METHOD__);

    $page = $request->query->getInt('page', 1);
    $limit = $request->query->getInt('limit', 5);

    $feeds = $this->feedManager->getAll($page, $limit);

    // Quitamos etiquetas HTML para la lista
    foreach($feeds as $feed) {
      $feed->setBody(strip_tags($feed->getBody()));
    }

    return $this->render('feed/list.html.twig', [
        'title' => 'Lista de Feeds',
        'feeds' => $feeds
    ]);
  }

  /**
   * Controlador de la vista y acción de creación.
   * @param Request $request
   * 
   * @Route("/create", methods={"GET", "POST"}, name="create-feed")
   */
  public function createAction(Request $request)
  {
    $this->logger->info('Fired '.__METHOD__);

    $feed = new Feed();
    $form = $this->createFormBuilder($feed)
      ->add('title', TextType::class, array(
        'label' => 'Título', 'attr' => array('class' => 'form-control')
        ))
      ->add('body', CKEditorType::class, array(
        'label' => 'Contenido del Feed', 'attr' => array('class' => 'form-control text-area-feed-content')
        ))
      ->add('save', SubmitType::class, array(
        'label' => 'Crear nuevo',
        'attr' => array( 'class' => 'btn btn-primary mt-3' )
        ))
      ->getForm();
    
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $feed = $form->getData();

      $em = $this->getDoctrine()->getManager();
      $em->persist($feed);
      $em->flush();

      return $this->redirectToRoute('get-feed', array('feedId' => $feed->getId()));
    }

    return $this->render('feed/create.html.twig', [
        'title' => 'Creación de Feed',
        'form' => $form->createView()
    ]);
  }

  /**
   * Controlador de la vista de detalle.
   * @param int $feedId ID del Feed a mostrar.
   * 
   * @Route("/detail/{feedId}", methods={"GET"}, name="get-feed")
   */
  public function getAction($feedId)
  {
    $this->logger->info('Fired '.__METHOD__);

    $feed = $this->getDoctrine()->getRepository('App:Feed')->findOneBy(array(
      "id" => $feedId,
      "deleted" => false
    ));

    $parameters = array();
    $parameters['title'] = (isset($feed) && !empty($feed->getId())) ? $feed->getTitle() : "No se ha encontrado el Feed indicado";
    $parameters['feed'] = $feed;
    
    return $this->render('feed/detail.html.twig', $parameters);

  }

  /**
   * Controlador de la vista y acción de edición.
   * @param Request $request
   * @param int $feedId ID del Feed a mostrar.
   * 
   * @Route("/edit/{feedId}", methods={"GET", "POST"}, name="update-feed")
   */
  public function editAction(Request $request, $feedId)
  {
    $this->logger->info('Fired '.__METHOD__);

    $feed = $this->getDoctrine()->getRepository('App:Feed')->findOneBy(array(
      "id" => $feedId,
      "deleted" => false
    ));
    
    $form = $this->createFormBuilder($feed)
      ->add('title', TextType::class, array(
        'label' => 'Título', 'attr' => array('class' => 'form-control')
        ))
      ->add('body',  CKEditorType::class, array(
        'label' => 'Contenido del Feed', 'attr' => array('class' => 'form-control text-area-feed-content')
        ))
      ->add('save', SubmitType::class, array(
        'label' => 'Guardar cambios',
        'attr' => array( 'class' => 'btn btn-primary mt-3' )
        ))
      ->getForm();
    
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      
      $em = $this->getDoctrine()->getManager();
      $em->flush();

      return $this->redirectToRoute('get-feed', array('feedId' => $feedId));
    }

    return $this->render('feed/edit.html.twig', [
        'title' => 'Edición de Feed',
        'form' => $form->createView()
    ]);
  }

  /**
   * Controlador de la petición de eliminación.
   * @param Request $request
   * @param int $feedId ID del Feed a eliminar.
   * 
   * @Route("/delete/{feedId}", methods={"DELETE"}, name="delete-feed")
   */
  public function deleteAction(Request $request, $feedId)
  {
    $this->logger->info('Fired '.__METHOD__);

    $feed = $this->getDoctrine()->getRepository('App:Feed')->find($feedId);

    if ( isset($feed) ) {

      // Eliminar y añadir fecha de modificación
      $feed->setDeleted(true);
      $feed->setDateModified(new \DateTime());

      $em = $this->getDoctrine()->getManager();
      $em->flush( $feed );
      $em->persist();

      $response = new Response();
      $response->send();

    } else {
      throw new BadRequestHttpException('No se ha podido obtener el Feed', null, 404);
    }
  }

  /**
   * Controlador de la acción de importación de Feeds desde los medios de prensa.
   * @param Request $request
   * 
   * @Route("/import", methods={"GET"}, name="import-feeds")
   */
  public function importFeedsAction()
  {
    $this->logger->info('Fired '.__METHOD__);
    $error = false;
    $errorMessage = "";

    try {
      
      // Parsear los medios de prensa en busca de Feeds
      $newFeedsCount = $this->feedReader->start();

    } catch (\Exception $e) {
        $error = true;
        $errorMessage .= "Se ha producido un error al importar las noticias.<br>";
        $errorMessage .= "ERROR:" . $e->getMessage();
    }

    return $this->render('feed/import.html.twig', [
        'title' => 'Importación de Feeds',
        'feeds_count' => $newFeedsCount,
        'error' => $error,
        'error_message' => $errorMessage
    ]);
  }
}
