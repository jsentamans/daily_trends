<?php

namespace App\Service;

use Psr\Log\LoggerInterface;
use Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;
use Symfony\Component\DomCrawler\Crawler;

use App\Manager\FeedManager;

class FeedReaderService
{
  private static $ROOT_DOCUMENTS = array(
    'mundo' => 'https://e00-elmundo.uecdn.es/elmundo/rss/portada.xml',
    'pais' => 'http://ep00.epimg.net/rss/elpais/portada.xml'
  );

  private $logger;
  private $feedManager;

  public function __construct(FeedManager $feedManager, LoggerInterface $logger)
  {
    $this->logger = $logger;
    $this->feedManager = $feedManager;
  }

  /**
   * Función encargada de lanzar los Web Crawlers y obtener los Feeds.
   * Según el parámetro del tipo, se busca en 'El País', 'El Mundo' o ambos.
   * 
   * @param string|null $pressType Objetivo del Web Crawler
   */
  public function start($pressType = null)
  {  
    $client = new Client();
    $newFeedsCount = 0;
    $feeds = array();

    // 'El mundo' Feeds
    if (!isset($pressType) || $pressType == 'mundo') {

      $rootUrl = self::$ROOT_DOCUMENTS['mundo'];
      $crawler = $client->request('GET', $rootUrl);

      $mainFeeds = $crawler->filter('rss channel item');
      $newFeeds = array();
      foreach ($mainFeeds as $mainFeed) {
        if (count($newFeeds) == 5) break;

        $indexTitle = $mainFeed->getElementsByTagName('title')->item(0)->textContent;
        $link = $mainFeed->getElementsByTagName('link')->item(0)->nodeValue;

        if (empty($link)) continue;

        $detailCrawler = $client->request('GET', $link);

        $bodyNode = $detailCrawler->filter('article');
        $body = (isset($bodyNode) && !empty($bodyNode)) ? $bodyNode->text() : '';

        $titleNode = $detailCrawler->filter('h1');
        $title = (isset($titleNode) && !empty($titleNode)) ? $titleNode->text() : '';

        $newFeeds[] = array(
          'index_title' => $indexTitle,
          'title' => $title,
          'body' => $body,
          'publisher' => 'El Mundo',
          'source' => $link
        );
      } // End foreach articles

      $feeds = array_merge($feeds, $newFeeds);
    } // End 'El mundo' Feeds

    // 'El pais' Feeds
    if (!isset($pressType) || $pressType == 'pais') {
      $rootUrl = self::$ROOT_DOCUMENTS['pais'];
      $crawler = $client->request('GET', $rootUrl);

      $mainFeeds = $crawler->filter('rss channel item');
      $newFeeds = array();
      foreach ($mainFeeds as $mainFeed) {
        if (count($newFeeds) == 5) break;

        $indexTitle = $mainFeed->getElementsByTagName('title')->item(0)->textContent;
        $link = $mainFeed->getElementsByTagName('link')->item(0)->textContent;

        if (empty($link)) continue;
        
        $detailCrawler = $client->request('GET', $link);

        $bodyNode = $detailCrawler->filter('#cuerpo_noticia');
        $body = (isset($bodyNode) && !empty($bodyNode)) ? $bodyNode->text() : '';

        $titleNode = $detailCrawler->filter('#articulo-titulo');
        $title = (isset($titleNode) && !empty($titleNode)) ? $titleNode->text() : '';

        $newFeeds[] = array(
          'index_title' => $indexTitle,
          'title' => $title,
          'body' => $body,
          'publisher' => 'El País',
          'source' => $link
        );
      } // End foreach articles

      $feeds = array_merge($feeds, $newFeeds);
    } // End 'El pais' Feeds

    if (!empty($feeds)) {
      $this->saveFeeds($feeds);
    }
    
    return count($feeds);
  }

  /**
   * Itera sobre los Feeds obtenidos, y lanza su creación usando el Manager.
   * @param Array $feeds
   */
  private function saveFeeds($feeds) {
    foreach($feeds as $feed) {
      $this->feedManager->createFeedFromCrawler($feed);
    }
  }

}
