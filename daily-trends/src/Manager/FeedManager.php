<?php

namespace App\Manager;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Feed;
use App\Repository\FeedRepository;

/**
 * Class FeedManager
 * @package AppBundle\Manager
 */
class FeedManager {

    protected $em;
    protected $container;
    protected $feedRepository;

    public function __construct(FeedRepository $feedRepository, EntityManagerInterface $entityManager, ContainerInterface $container){
        $this->container = $container;
        $this->em = $entityManager;
        $this->feedRepository = $feedRepository;
    }

    /**
     * Obtiene todos los registros (con deleted = '0'), y pagina los resultados a través del límite y número de página.
     * @param int $page     Número de página
     * @param int $limit    Número de registros por página
     */
    public function getAll($page = 1, $limit = 5) {

        $feedEntities = $this->feedRepository->findBy(array(
            "deleted" => false
        ));
        $paginator = $this->container->get('knp_paginator');
        $results = $paginator->paginate(
            $feedEntities,
            $page,
            $limit
        );
        return $results;
    }

    /**
     * Creación de un Feed a través de la información obtenida desde el Web Crawler.
     * @param Array $feed   [index_title, title, body, source, publisher]
     */
    public function createFeedFromCrawler($feed)
    {
        $feedEntity = new Feed();

        $feedEntity->setTitle($feed['title']);
        $feedEntity->setBody($feed['body']);
        $feedEntity->setSource($feed['source']);
        $feedEntity->setPublisher($feed['publisher']);

        $this->em->persist($feedEntity);
        $this->em->flush();
    }
}
