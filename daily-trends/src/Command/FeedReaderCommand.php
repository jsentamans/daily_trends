<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

use App\Service\FeedReaderService;
use Psr\Log\LoggerInterface;

class FeedReaderCommand extends Command
{
    //protected static $defaultName = 'feed:reader';
    private $feedReader;
    private $logger;

    public function __construct(FeedReaderService $feedReader, LoggerInterface $logger) {
        $this->feedReader = $feedReader;
        $this->logger = $logger;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('feed:reader')
            ->setDescription('Obtiene los feeds recientes de las webs de los medios de prensa')
            ->addArgument('press', InputArgument::OPTIONAL, 'Tipo de medio. Valores: [mundo] | [pais] | [] (todos)')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $pressType = $input->getArgument('press');
        
        try {
            if (isset($pressType) && !empty($pressType) && !in_array($pressType, array('mundo', 'pais'))) {
                throw new \Exception("El tipo del medio de prensa '{$pressType}' indicado no es un valor correcto.");
            }

            // Parsear los medios de prensa en busca de Feeds
            $newFeedsCount = $this->feedReader->start($pressType);

            $io->success("Proceso completado con éxito! Se han creado '{$newFeedsCount}' nuevos Feeds.");

        } catch (\Exception $e) {
            $io->error("Error al obtener los Feeds. ERROR: '{$e->getMessage()}'");
        }
    }
}
