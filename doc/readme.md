
# Detalles del desarrollo

## Instalación de dependencias

```sh
# Bundles
composer install

# Node modules
yarn install
```

## Base de datos

Conexión base de datos:
* create database daily_trends;
* grant all on daily_trends.* to 'daily_user'@'localhost' identified by 'd@ilytr3nds';

Actualización esctructura DB:

```sh
# Ver cambios
$ php bin/console doctrine:schema:update --dump-sql

# Actualizar estructura
$ php bin/console doctrine:schema:update --force
```

## Build de archivos scss y js (Webpack)

Compilar ficheros scss y javascript:

```sh
yarn run encore dev
```
